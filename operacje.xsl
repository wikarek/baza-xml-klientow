<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<html>
    <title>Strona sklepu</title>
<body>
    <h1>Sklep</h1>
    <xsl:apply-templates/>
</body>
</html>
</xsl:template>

<xsl:template match="Klienci">
    <h1>Tabela  Klient</h1>
    <table>
        <tr>
            <td>ID</td>
            <td>Imie</td>
            <td>Nazwisko</td>
        </tr>
        <xsl:for-each  select="Klient" >
        <tr>
            <td> <xsl:value-of select="@id_k"/> </td>
            <td> <xsl:value-of select="@imie"/> </td>
            <td> <xsl:value-of select="@nazwisko"/> </td>
        </tr>
        </xsl:for-each>
    </table>      
</xsl:template>

<xsl:template match="zakupy">
    <h1>Tabela Towary</h1>
    <table>
        <tr>
            <td>ID</td>
            <td>Nazwa</td>
            <td>Cena</td>
        </tr>
        <xsl:for-each  select="towar" >
        <tr>
            <td> <xsl:value-of select="id"/> </td>
            <td> <xsl:value-of select="nazwa"/> </td>
            <td> <xsl:value-of select="cena"/> </td>
        </tr>
        </xsl:for-each>
    </table>
</xsl:template>

<xsl:template match="Klient">
    <h1>Tabela Sprzedaz</h1>
    <table>
        <tr>
            <td>ID:</td>
            <td>Data:</td>
        </tr>
        <xsl:for-each  select="zakupy" >
        <tr>
            <td> <xsl:value-of select="@id"/> </td>
            <td> <xsl:value-of select="@data"/>  </td>
        </tr>
          </xsl:for-each>
    </table>
</xsl:template>


<xsl:template match="Klienci">
    <h1>Tabela Zakupow Klientow</h1>
    <table>
        <tr>
            <td>ID</td>
            <td>Imie klienta</td>
            <td>Nazwisko Klienta</td>
            <td>Data_zakupow</td>
            <td>Nazwa produktu</td>
            <td>Cena produktu</td>
        </tr>
        <xsl:for-each  select="Klient" >
        <tr>
            <td>  <xsl:value-of select="@id"/> </td>
            <td> <xsl:value-of select="@imie"/> </td>
            <td> <xsl:value-of select="@nazwisko"/> </td>
            <td> <xsl:value-of select="/sprzedaz/@data_zakupow"/> </td>
            <td> <xsl:value-of select="/towar/nazwa"/> </td>
            <td> <xsl:value-of select="/towar/cena"/>  </td>
        </tr>
         </xsl:for-each>
    </table>
</xsl:template>

</xsl:stylesheet>

