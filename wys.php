<?php
    $Klienci = simplexml_load_file('klient.xml');
?>
<!DOCTYPE HTML>
<html lag="pl">
    <head>
        <title>Strona obslugujaca XML</title>
        <meta charset="utf-8" />
        <link rel="stylesheet"  href="style.css" type="text/css"/>
    </head>
    <body>
    <h2>Wyswietl towary zakupione przez danego klienta</h2>
    <?php if($_SERVER['REQUEST_METHOD'] == 'GET' ) {?>
        <form method="POST" action="wys.php">
            <select name="klienci">
            <?php foreach($Klienci->Klient as $klient){?>
                    <option value="<? echo $klient['id_k'] ?>"> <? echo $klient['imie'] . " " . $klient['nazwisko']?></option> 
            <?}?>
            </select>
            <input type="submit" value="Wybierz"/>
        </form>
    <?} else{?>
        <table>
            <tr>
                <td>Nazwa towaru</td>
                <td>Cena Towaru</td>
            </tr>
            <tr>
                <?php foreach($Klienci->Klient as $klient){ 
                    if($klient['id_k'] == $_POST['klienci'] ){  ?>
                        <?php foreach($Klienci->Klient[$klient['id_k'] - 1]->zakupy as $zakup ){  $zak_ind = $zakup['id']; ?>
                         <?php foreach($zakup->towar as $towary ){ ?>
                            <tr>
                                <td><? echo $towary->nazwa ?></td>
                                <td><?  echo $towary->cena ?> </td>
                            </tr>
                         <? }?>
                        <? }?>
                    <?} ?>
                <?} ?>
            </tr>
        </table>
        <a href="wys.php">Powrot</a>
    <?}?>    
    </body>
</html>